CREATE TABLE saved_games (
    id UUID PRIMARY KEY,
    game_name TEXT UNIQUE NOT NULL,
    game_str TEXT NOT NULL,
    width INT NOT NULL,
    height INT NOT NULL,
    time INT NOT NULL,
    date TIMESTAMP NOT NULL,
    mines INT NOT NULL,
    date_created TIMESTAMP NOT NULL
);