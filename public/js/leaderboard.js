function build_list(listname, data) {
    let list = document.getElementById(listname);
    for (let i = 0; i < data.length; ++i) {
        let tr = document.createElement('tr');
        tr.classList.add('leader');

        let num = document.createElement('td'); num.innerHTML = (i+1) + '.'
        tr.appendChild(num);

        let username = document.createElement('td');
        username.innerHTML = data[i].username ||'<i>unknown</i>';
        tr.appendChild(username);
        
        let time = document.createElement('td');
        let minutes = Math.floor(data[i].time/60);
        let seconds = data[i].time%60;
        time.innerHTML = minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
        tr.appendChild(time);
        
        let difficulty = document.createElement('td');
        difficulty.innerHTML = (data[i].difficulty/1000);
        tr.appendChild(difficulty);

        list.appendChild(tr);
    }
}

function clear_boards() {
    let leaders = document.getElementsByClassName('leader');
    while (leaders.length) {
        leaders[0].remove();
    }
}

function get_leaderboards(range) {
    $.ajax({
        url: '/get_leaderboards',
        type: 'GET',
        cache: false,
        data: {
            range: range
        },
        success: function(data) {
            clear_boards();
            build_list('easy', data.easy)
            build_list('intermediate', data.intermediate)
            build_list('expert', data.expert)
        },
        error: function(jqXHR, textStatus, err) {
            console.error('Error: ', jqXHR.status, jqXHR.responseText, textStatus, err);
        }
    });
}

// start leaderboards on this week
get_leaderboards(7);

document.getElementById('range').onchange = function(ev) {
    get_leaderboards(ev.target.value);
}
