// save game

function build_board_state() {
    let board_state = [];
    for (let y = 0; y < height; ++y) {
        let row_state = [];
        for (let x = 0; x < width; ++x) {
            let cell_model = board_model[y][x];
            let cell_view = board_view.children[y].children[x];
            let flagged = Number(cell_view.classList.contains('flagged'));
            let revealed = Number(cell_view.classList.contains('revealed'));

            let cell_state = {
                val: cell_model,
                flagged: flagged,
                revealed: revealed
            };
            row_state.push(cell_state);
        }
        board_state.push(row_state);
    }
    return board_state;
}

function save_game_to_db(type, board_state, game_name) {
    $.ajax({
        url: '/' + type + '_game',
        type: 'POST',
        cache: false,
        data: {
            board: JSON.stringify(board_state),
            mines: mines,
            time: 60*minutes + seconds,
            name: game_name
        },
        success: function(data) {
            if (data != 'OK') {
                alert('Sorry, a game with that name already exists, please choose a different one.');
                save_game();
            }
            else {
                alert('Game state ' + type + 'd. Use ' + game_name + ' to resume playing this game.');
                localStorage.setItem('game_name', game_name);
                playing_saved_game = true;
            }
        },
        error: function(jqXHR, textStatus, err) {
            alert('Sorry, we are currently unable to save games. Please try again later.');
            console.error('Error: ', jqXHR.status, jqXHR.responseText, textStatus, err);
        }
    });
}

function save_game() {
    // let game_name = localStorage.getItem('game_name');
    let game_name;
    let board_state = build_board_state();
    if (game_name != '' && playing_saved_game) {
        save_game_to_db('update', board_state, game_name);
    }
    else if (game_name = prompt("Please enter a name to resume this game later: ")) {
        save_game_to_db('save', board_state, game_name)
    }
    else {
        alert('Game not saved');;
    }
}

// end save game

// random clicker

function click_random(playing_new_game) {
    if (playing_new_game) {
        row = Math.floor(Math.random() * height);
        col = Math.floor(Math.random() * width);

        board_model = build_board(height, width, mines, row, col);

        timer();
        set_game_ctrl_display('block');
        first_click = false;
        game_started = true;
    }
    else {
        if (row > -1 && col > -1) {
            board_view.children[row].children[col].classList.remove('selected-cell');
        }
        else {
            // if we are clicking random to continue a saved game
            timer();
            game_started = true;
        }

        row = Math.floor(Math.random() * height);
        col = Math.floor(Math.random() * width);

        let cell_view = board_view.children[row].children[col];

        while (cell_view.classList.contains('revealed') || cell_view.classList.contains('flagged')) {
            row = Math.floor(Math.random() * height)
            col = Math.floor(Math.random() * width);
            cell_view = board_view.children[row].children[col];
        }
    }

    left_mouse_click(row, col);
    board_view.children[row].children[col].classList.add('selected-cell');
}

// end random clicker

// hint helper

function give_hint() {
    let attempts = 0;

    let row = Math.floor(Math.random() * height)
    let col = Math.floor(Math.random() * width);

    let cell_model = board_model[row][col];
    let cell_view = board_view.children[row].children[col];

    while (cell_model == ' ' || cell_model == '💣' || cell_view.classList.contains('revealed') || (!is_bordered(row, col) && attempts < 2*height*width)) {
        row = Math.floor(Math.random() * height)
        col = Math.floor(Math.random() * width);
        cell_model = board_model[row][col];
        cell_view = board_view.children[row].children[col];

        attempts++;
    }

    cell_view.classList.add('blinker');
    setTimeout(() => {
        cell_view.classList.remove('blinker');
    }, 2000)

    highscore_eligible = false;
}

function is_bordered(row, col) {
    let above = row > 0 && board_model[row - 1][col] > 0 && board_view.children[row - 1].children[col].classList.contains('revealed');
    let below = row < height - 1 && board_model[row + 1][col] > 0 && board_view.children[row + 1].children[col].classList.contains('revealed');
    let left = col > 0 && board_model[row][col - 1] > 0 && board_view.children[row].children[col - 1].classList.contains('revealed');
    let right = col < width - 1 && board_model[row][col + 1] > 0 && board_view.children[row].children[col + 1].classList.contains('revealed');

    return above || below || left || right;
}

// end hint helper

// solve helper

function solve_board() {
    highscore_eligible = false;

    let click_coords = [];
    for (let row = 0; row < height; ++row) {
        for (let col = 0; col < width; ++col) {
            let cell_view = board_view.children[row].children[col];

            if (cell_view.classList.contains('revealed') && cell_view.children[0].innerHTML != ' ') {
                let flags = count_surrounding_class(row, col, 'flagged');
                let unrevealed = count_surrounding_class(row, col, 'unrevealed');
                let cell_val = Number(cell_view.children[0].innerHTML);

                if (flags + unrevealed == cell_val && unrevealed != 0) {
                    click_coords = click_coords.concat(click_surrounding_unrevealed(row, col, 'unrevealed', true));
                }
                else if (flags == cell_val && unrevealed != 0) {
                    click_coords = click_coords.concat(click_surrounding_unrevealed(row, col, 'unrevealed', false));
                }
            }

        }
    }

    let index = 0;
    let clickint = setInterval(function() {
        if (game_over) {
            clearInterval(clickint);
            return;
        }

        if (index >= click_coords.length) {
            clearInterval(clickint);
            if (click_coords.length > 0) {
                solve_board();
            }
            return;
        }

        let coord = click_coords[index];

        let mousedown = new Event('mousedown');
        if (coord.flag) {
            mousedown.which = 3;
        }

        let cell_view = board_view.children[coord.row].children[coord.col];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            cell_view.dispatchEvent(mousedown);
            clicked = true;
        }

        index += 1;
    }, 1)
}

function count_surrounding_class(row, col, class_name) {
    let count = 0;
    if (row > 0 && board_view.children[row - 1].children[col].classList.contains(class_name)) {
        count += 1;
    }
    if (row < height - 1 && board_view.children[row + 1].children[col].classList.contains(class_name)) {
        count += 1;
    }
    if (col > 0 && board_view.children[row].children[col - 1].classList.contains(class_name)) {
        count += 1;
    }
    if (col < width - 1 && board_view.children[row].children[col + 1].classList.contains(class_name)) {
        count += 1;
    }
    if (row > 0 && col > 0 && board_view.children[row - 1].children[col - 1].classList.contains(class_name)) {
        count += 1;
    }
    if (row < height - 1 && col < width - 1 && board_view.children[row + 1].children[col + 1].classList.contains(class_name)) {
        count += 1;
    }
    if (row > 0 && col < width - 1 && board_view.children[row - 1].children[col + 1].classList.contains(class_name)) {
        count += 1;
    }
    if (row < height - 1 && col > 0 && board_view.children[row + 1].children[col - 1].classList.contains(class_name)) {
        count += 1;
    }
    return count;
}

function click_surrounding_unrevealed(row, col, class_name, flag) {
    let reveal_coords = [];
    if (row > 0 && board_view.children[row - 1].children[col].classList.contains(class_name)) {
        let cell_view = board_view.children[row - 1].children[col];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row - 1, col: col, flag: flag });
        }
    }
    if (row < height - 1 && board_view.children[row + 1].children[col].classList.contains(class_name)) {
        let cell_view = board_view.children[row + 1].children[col];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row + 1, col: col, flag: flag });
        }
    }
    if (col > 0 && board_view.children[row].children[col - 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row].children[col - 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row, col: col - 1, flag: flag });
        }
    }
    if (col < width - 1 && board_view.children[row].children[col + 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row].children[col + 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row, col: col + 1, flag: flag });
        }
    }
    if (row > 0 && col > 0 && board_view.children[row - 1].children[col - 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row - 1].children[col - 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row - 1, col: col - 1, flag: flag });
        }
    }
    if (row < height - 1 && col < width - 1 && board_view.children[row + 1].children[col + 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row + 1].children[col + 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row + 1, col: col + 1, flag: flag });
        }
    }
    if (row > 0 && col < width - 1 && board_view.children[row - 1].children[col + 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row - 1].children[col + 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row - 1, col: col + 1, flag: flag });
        }
    }
    if (row < height - 1 && col > 0 && board_view.children[row + 1].children[col - 1].classList.contains(class_name)) {
        let cell_view = board_view.children[row + 1].children[col - 1];
        if (!cell_view.classList.contains('revealed') && !cell_view.classList.contains('flagged')) {
            reveal_coords.push({ row: row + 1, col: col - 1, flag: flag });
        }
    }
    return reveal_coords;
}

// end solve helper
