// update theme
let color = localStorage.getItem('theme') || 'blue';
document.getElementById('theme-switch').checked = (color == 'blue');

document.addEventListener('keypress', (ev) => {
    if (ev.which == 13) {
        start_game();
    }
})

document.getElementById('start-button').addEventListener('click', () => {
    start_game();
})

function change_theme() {
    let is_blue = (theme == 'blue');
    document.body.style.backgroundColor = (is_blue ? '#ffcfdc' : '#D8DEE9');
    document.body.style.color = (is_blue ? '#ca7b80' : '#434C5E');
    Array.from(document.getElementsByTagName('button')).map(button => button.style.color = (is_blue ? '#696969' : '#434C5E'));
    theme = (is_blue ? 'pink' : 'blue');
    localStorage.setItem('theme', theme);
}

function start_game() {
    let level;
    let height;
    let width;
    let mines;

    let saved_game = document.getElementById('resume').value
    if (saved_game != '') {
        $.ajax({
            url: '/get_saved_game',
            type: 'GET',
            cache: false,
            data: {
                name: saved_game
            },
            success: function(game) {
                if (game.rowCount == 0) {
                    alert('Sorry, no game with that name was found.');
                    document.getElementById('resume').value = '';
                    return;
                }
                else {
                    localStorage.setItem('height', game.rows[0].height);
                    localStorage.setItem('width', game.rows[0].width);
                    localStorage.setItem('mines', game.rows[0].mines);
                    localStorage.setItem('playing-saved-game', true);
                    localStorage.setItem('board_str', game.rows[0].game_str);
                    localStorage.setItem('saved_time', game.rows[0].time);
                    localStorage.setItem('game_name', game.rows[0].game_name);

                    location = '/minesweeper.html';
                }
            },
            error: function(jqXHR, textStatus, err) {
                console.error("Error: ", jqXHR.status, jqXHR.responseText, textStatus, err);
            }
        });
    }
    else {
        level = document.getElementById('difficulty-select').value;
        if  (document.getElementById('mines').value != '' && document.getElementById('height').value != '' && document.getElementById('width').value != '') {
            level = 'custom';
        }
    
        const max_height = 30;
        const max_width = 30;
    
        switch (level) {
            case 'custom':
                height = document.getElementById('height').value;
                width = document.getElementById('width').value;
                mines = document.getElementById('mines').value;
                break;
            case 'easy':
                height = 9;
                width = 9;
                mines = 10;
                break;
            case 'intermediate':
                height = 16;
                width = 16;
                mines = 40;
                break;
            case 'expert':
                height = 16;
                width = 30;
                mines = 99;
                break;
        }
    
        if (height < 1) {
            height = 9;
        }
        else if (height > 30) {
            height = max_height;
        }
        if (width < 1) {
            width = 9;
        }
        else if (width > max_width) {
            width = max_width
        }
        if (mines < 0 || mines > ((height*width)-9)) {
            mines = Math.floor(0.1 * height * width);
        }

        localStorage.setItem('height', height);
        localStorage.setItem('width', width);
        localStorage.setItem('mines', mines);
        localStorage.setItem('theme', theme);
        localStorage.setItem('playing-saved-game', false);

        location = '/minesweeper.html';
    }
}
