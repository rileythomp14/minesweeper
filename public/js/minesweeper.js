// start setup

let playing_saved_game = (localStorage.getItem('playing-saved-game') == 'true');
let saved_time = Number(localStorage.getItem('saved_time'));
let height = Number(localStorage.getItem('height'));
let width = Number(localStorage.getItem('width'));
let mines = Number(localStorage.getItem('mines'));
let theme = localStorage.getItem('theme');
let is_pink = (theme == 'pink');

let t;
let seconds = (playing_saved_game ? saved_time%60 : 0);
let minutes = (playing_saved_game ? Math.floor(saved_time/60) : 0);

let row = -1;
let col = -1;

let revealed_cells = 0;
let flag_count = 0;

let has_lost = false;

let highscore_eligible = !playing_saved_game;

let first_click = !playing_saved_game;

let game_over = false;

let game_started = false;

let board_view = document.getElementById('board');
let board_model;

if (playing_saved_game) {
    let clock = document.getElementById('timer');
    clock.innerHTML = format_time(minutes) + ':' + format_time(seconds);
    board_model = board_model_from_str();
    board_view_from_str();
    set_game_ctrl_display('block');
}

for (let i = 0; i < height && !playing_saved_game; ++i) {
    let row = document.createElement('tr');
    row.setAttribute('class', 'row');
    row.setAttribute('id', 'row' + i)

    for (let j = 0; j < width; ++j) {
        let cell = document.createElement('td');
        cell.setAttribute('class', 'cell');
        cell.setAttribute('id', 'cell' + j);
        cell.classList.add('unrevealed');
        cell.addEventListener('mousedown', handle_click);
        row.appendChild(cell);
    }

    board_view.appendChild(row)
}

document.body.style.backgroundColor = (is_pink ? '#ffcfdc' : '#D8DEE9');
document.body.style.color = (is_pink ? '#ca7b80' : '#434C5E');
if (!playing_saved_game) { 
    Array.from(document.getElementsByTagName('button')).map(button => button.style.color = (is_pink ? '#696969' : '#434C5E'));
    Array.from(document.getElementsByTagName('td')).map(td => td.style.backgroundColor = (is_pink ? '#d86386' : '#3B4252'));
}

document.getElementById('bomb-count').innerHTML = mines;
document.getElementById('flag-count').innerHTML = flag_count;

// end setup

// event listeners

document.getElementById('give-up').addEventListener('click', give_up);
document.getElementById('hint').addEventListener('click', give_hint);
document.getElementById('solve').addEventListener('click', solve_board);
document.getElementById('save-game').addEventListener('click', save_game);

function move_selected_cell(delta_row, delta_col) {
    board_view.children[row].children[col].classList.remove('selected-cell');
    row += delta_row
    col += delta_col
    board_view.children[row].children[col].classList.add('selected-cell');
}


document.body.onkeydown = function(e) {
    e = e || window.event;

    if (game_over) {
        if (e.keyCode == '13') {
            restart_game();
        }
        return;
    }
    else if (e.keyCode == '68') {
        // d, simulate random human click
        click_random(first_click);
    }
    if (!first_click) {
        if (e.keyCode == '38') {
            // up arrow
            e.preventDefault();
            if (row > 0) {
                move_selected_cell(-1, 0);
            }
        }
        else if (e.keyCode == '40') {
            // down arrow
            e.preventDefault();
            if (row < height - 1) {
                move_selected_cell(1, 0);
            }
        }
        else if (e.keyCode == '37') {
            // left arrow
            e.preventDefault();
            if (col > 0) {
                move_selected_cell(0, -1);
            }
        }
        else if (e.keyCode == '39') {
            // right arrow
            e.preventDefault();
            if (col < width - 1) {
                move_selected_cell(0, 1);
            }
        }
        else if (e.keyCode == '70') {
            // f key, flag cell
            if (board_view.children[row].children[col].children.length == 0) {
                let flag = document.createElement('span');
                flag.innerHTML = '🚩';
                board_view.children[row].children[col].classList.add('flagged');
                board_view.children[row].children[col].classList.remove('unrevealed');
                board_view.children[row].children[col].appendChild(flag);
                document.getElementById('flag-count').innerHTML = ++flag_count;
            }
            else if (board_view.children[row].children[col].children[0].innerHTML == '🚩') {
                let flag = board_view.children[row].children[col].children[0];
                board_view.children[row].children[col].removeChild(flag);
                document.getElementById('flag-count').innerHTML = --flag_count;
            }
        }
        else if (e.keyCode == '32') {
            //space bar, treat as left click
            e.preventDefault();
            left_mouse_click(row, col);
        }
        else if (e.keyCode == '81' && confirm('Are you sure you want to quit?')) {
            // q, give up
            give_up();
        }
        else if (e.keyCode == '83') {
            // s, solve board
            if (!game_started) {
                timer();
                game_started = true;
            }
            solve_board();
        }
        else if (e.keyCode == '65') {
            // a, give hint
            give_hint();
            if (!game_started) {
                timer();
                game_started = true;
            }
        }
        else if (e.keyCode == '87') {
            save_game();
        }
    }
}

// prevent menu popup on right click for flag
board_view.addEventListener('contextmenu', (ev) => {
    ev.preventDefault()
});

// end event listeners

function set_game_ctrl_display(display) {
    document.getElementById('give-up').style.display = display;
    document.getElementById('hint').style.display = display;
    document.getElementById('solve').style.display = display;
    document.getElementById('save-game').style.display = display;
}

function board_model_from_str() {
    let str = localStorage.getItem('board_str');
    let capx = 0;
    let board = [];
    for (let y = 0; y < height; ++y) {
        let row = []
        for (let x = 0; x < width; ++x) {
            if (str[y*width + x + capx] == 'X') {
                capx++;
            }
            let c = str[y*width + x + capx];
            let val;
            if (c == '@' || c == 'x') {
                val = '💣';
            }
            // if upper case, convert to int
            if (c.charCodeAt(0) < 'a'.charCodeAt(0) && val != '💣') {
                val = c.charCodeAt(0) - 'A'.charCodeAt(0)
            }
            // else if lower case convert to int
            else if (val != '💣') {
                val = c.charCodeAt(0) - 'a'.charCodeAt(0)
            }

            row.push(val);
        }
        board.push(row);
    }
    return board;
}

function board_view_from_str() {
    let str = localStorage.getItem('board_str');
    let capx = 0;

    for (let i = 0; i < height; ++i) {
        let row = document.createElement('tr');
        row.setAttribute('class', 'row');
        row.setAttribute('id', 'row' + i)
    
        for (let j = 0; j < width; ++j) {
            if (str[i*width + j + capx] == 'X') {
                capx++;
            }
            let cell_model = board_model[i][j];
            let cell_view = document.createElement('td');
            cell_view.setAttribute('class', 'cell');
            cell_view.setAttribute('id', 'cell' + j);
            let cell_val = document.createElement('span');
            if (cell_model == '💣' || str[i*width + j + capx].charCodeAt(0) < 'a'.charCodeAt(0) || cell_val.innerHTML == '🚩') {
                cell_view.classList.add('unrevealed');
                cell_view.style.backgroundColor = (is_pink ? '#d86386' : '#3B4252');
                cell_val.innerHTML = '';
            }
            else {
                revealed_cells++;
                cell_view.classList.add('revealed');
                cell_view.style.backgroundColor = (is_pink ? '#f199b3' : '#4C566A');
                cell_val.style.color = (is_pink ? 'black' : '#A3BE8C');
                cell_val.innerHTML = (cell_val.innerHTML != '🚩' && cell_model == 0 ? ' ' : cell_model);
                cell_view.appendChild(cell_val);
            }
            if (str[i*width + j + capx] == 'x') {
                cell_val.innerHTML = '🚩';
                cell_view.classList.add('flagged');
                cell_view.classList.remove('unrevealed');
                cell_view.appendChild(cell_val);
            }
            if (str[i*width + j + capx - 1] == 'X') {
                cell_val.innerHTML = '🚩';
                cell_view.classList.add('flagged');
                cell_view.classList.remove('unrevealed');
                cell_view.appendChild(cell_val);
            }
        
            cell_view.addEventListener('mousedown', handle_click);
            row.appendChild(cell_view);
        }
    
        board_view.appendChild(row)
    }

    flag_count = document.getElementsByClassName('flagged').length;
}

function on_default_levels() {
    return (width == 9 && height == 9 && mines == 10) || (width == 16 && height == 16 && mines == 40) || (width == 30 && height == 16 && mines == 99);
}

function board_score() {
    let total = 0;
    for (let i = 0; i < height; ++i) {
        for (let j = 0; j < width; ++j) {
            if (board_model[i][j] != '💣') {
                total += board_model[i][j]

            }
        }
    }
    return (total/(width*height)).toFixed(3);
}

function reset_cells() {
    for (let i = 0; i < height; ++i) {
        for (let j = 0; j < width; ++j) {
            let cell = board_view.children[i].children[j];

            if (cell.children.length != 0) {
                let child = cell.children[0];
                cell.removeChild(child);
            }

            cell.innerHTML = '';
            cell.style.backgroundColor = (is_pink ? '#d86386' : '#3B4252');
            cell.classList.remove('revealed');
            cell.classList.remove('flagged');
            cell.classList.remove('selected-cell');
            cell.classList.add('unrevealed');
            cell.addEventListener('mousedown', handle_click);
        }
    }
}

function restart_game() {
    game_started = false;
    playing_saved_game = false;
    highscore_eligible = true;
    row = -1;
    col = -1;
    has_lost = false;
    game_over = false;
    revealed_cells = 0;
    document.getElementById('game-msg').innerHTML = '';
    document.getElementById('board-score').innerHTML = '';
    document.getElementById('new-game').style.display = 'none';
    reset_cells();

    seconds = 0;
    minutes = 0;
    document.getElementById('timer').innerHTML = '00:00';
    first_click = true;
    flag_count = 0;
    document.getElementById('flag-count').innerHTML = flag_count;
    localStorage.setItem('playing-saved-game', false);
}

function give_up() {
    has_lost = true;
    game_over = true;
    document.getElementById('game-msg').innerHTML = 'You lost';
    document.getElementById('board-score').innerHTML = 'Board difficulty: ' + board_score()
    clearTimeout(t);

    let button = document.getElementById('new-game');
    button.innerHTML = 'Play again';
    button.style.display = 'inline-block';
    set_game_ctrl_display('none');

    for (let i = 0; i < height; ++i) {
        for (let j = 0; j < width; ++j) {
            let cell_view = board_view.children[i].children[j];
            let cell_model = board_model[i][j];
            let cell_val = document.createElement('span');

            if (cell_view.children.length != 0 && cell_view.children[0].innerHTML == '🚩') {
                let child = cell_view.children[0];
                cell_view.removeChild(child);
            }

            if (!cell_view.classList.contains('revealed')) {
                update_cell_view(cell_view, cell_val, cell_model);;
            }
        
            cell_view.removeEventListener('mousedown', handle_click);
        }
    }

    button.addEventListener('click', restart_game);
}

function add_to_leaderboard() {
    $.ajax({
        url: '/is_highscore',
        type: 'GET',
        cache: false,
        data: {
            level: Math.ceil(width/10),
            time: 60*minutes + seconds,
        },
        success: function(is_highscore) {
            if (is_highscore) {
                let username = prompt("Congrats! You got a new high score! Please enter your name: ");
                $.ajax({
                    url: '/add_game',
                    type: 'POST',
                    cache: false,
                    data: {
                        level: Math.ceil(width/10),
                        time: 60*minutes + seconds,
                        name: username,
                        difficulty: board_score()*1000
                    },
                    success: function(data) {
                        if (data != 'OK') {
                            alert('Sorry, we are unable to add games to the leaderboards at this time');
                        }
                    },
                    error: function(jqXHR, textStatus, err) {
                        console.error("Error: ", jqXHR.status, jqXHR.responseText, textStatus, err);
                    }
                });
            }
        },
        error: function(jqXHR, textStatus, err) {
            console.error("Error: ", jqXHR.status, jqXHR.responseText, textStatus, err);
        }
    });
}

function update_cell_view(cell_view, cell_val, cell_model) {
    cell_val.innerHTML = (cell_model == 0 ? ' ' : cell_model);
    if (cell_view.children.length > 0 && cell_view.children[0].innerHTML == '🚩') {
        cell_view.removeChild(cell_view.children[0]);
        document.getElementById('flag-count').innerHTML = --flag_count;
    }
    cell_view.appendChild(cell_val);
    cell_view.style.backgroundColor = (is_pink ? '#f199b3' : '#4C566A');
    cell_val.style.color = (is_pink ? 'black' : '#A3BE8C');
    cell_view.classList.add('revealed');
    cell_view.classList.remove('unrevealed');

    if (!has_lost && revealed_cells == height*width - mines) {
        game_over = true;

        document.getElementById('game-msg').innerHTML = 'You won!';
        document.getElementById('board-score').innerHTML = 'Board difficulty: ' + board_score()
        clearTimeout(t);

        let button = document.getElementById('new-game');
        button.innerHTML = 'Play again';
        button.style.display = 'inline-block';

        set_game_ctrl_display('none');

        for (let i = 0; i < height; ++i) {
            for (let j = 0; j < width; ++j) {
                let cell = board_view.children[i].children[j];
                cell.removeEventListener('mousedown', handle_click);
            }
        }

        if (on_default_levels() && highscore_eligible) {
            add_to_leaderboard();
        }

        button.addEventListener('click', restart_game);
    }
}

function reveal_cells(row, col) {
    let cell_model = board_model[row][col];
    let cell_view = board_view.children[row].children[col];
    let cell_val = document.createElement('span');

    if (cell_view.classList.contains('revealed')) {
        return;
    }

    revealed_cells += 1;

    update_cell_view(cell_view, cell_val, cell_model);

    if (cell_model != '') {
        return
    }
    else {
        if (row > 0) {
            reveal_cells(row - 1, col);
        }
        if (row < height-1) {
            reveal_cells(row + 1, col);
        }
        if (col > 0) {
            reveal_cells(row, col - 1);
        }
        if (col < width-1) {
            reveal_cells(row, col + 1);
        }
        if (row > 0 && col > 0) {
            reveal_cells(row-1, col-1);
        }
        if (row < height-1 && col < width - 1) {
            reveal_cells(row+1, col+1);
        }
        if (row > 0 && col < width - 1) {
            reveal_cells(row-1, col+1);
        }
        if (row < height - 1 && col > 0) {
            reveal_cells(row+1, col-1);
        }
    }
}

function left_mouse_click(row, col) {
    // left mouse click
    if (board_model[row][col] == '💣') {
        if (!confirm("Are you sure you want to click there?")) {
            highscore_eligible = false;
            return;
        }
        has_lost = true;
        game_over = true;
        document.getElementById('game-msg').innerHTML = 'You lost';
        document.getElementById('board-score').innerHTML = 'Board difficulty: ' + board_score();
        clearTimeout(t);

        let button = document.getElementById('new-game');
        button.innerHTML = 'Play again';
        button.style.display = 'inline-block';
        set_game_ctrl_display('none');

        for (let i = 0; i < height; ++i) {
            for (let j = 0; j < width; ++j) {
                let cell_view = board_view.children[i].children[j];
                let cell_model = board_model[i][j];
                let cell_val = document.createElement('span');

                if (cell_view.children.length != 0 && cell_view.children[0].innerHTML == '🚩') {
                    let child = cell_view.children[0];
                    cell_view.removeChild(child);
                }

                if (!cell_view.classList.contains('revealed')) {
                    update_cell_view(cell_view, cell_val, cell_model);
                }
            
                cell_view.removeEventListener('mousedown', handle_click);
            }
        }

        button.addEventListener('click', restart_game);
    }

    reveal_cells(row, col);
}

// cell_view is the <td></td> element
// cell_val is the <span></span> inside of cell_view
// cell_model is the cell value in the matrix representation
function handle_click(ev) {
    if (!first_click && row > -1 && col > -1) {
        board_view.children[row].children[col].classList.remove('selected-cell');
    }

    // row and col clicked on
    let target = ev.target;
    if (ev.target.tagName == 'SPAN') {
        target = target.parentElement;
    }
    row = Number(target.parentNode.id.replace('row', ''));
    col = Number(target.id.replace('cell', ''));
    
    

    if (first_click && !game_started) {
        board_model = build_board(height, width, mines, row, col);

        timer();
        set_game_ctrl_display('block');
        first_click = false;
        game_started = true;
    }
    else if (playing_saved_game && !game_started) {
        timer();
        game_started = true;
    }

    // right mouse click
    if (ev.which == 3) {
        if (target.children.length == 0) {
            let flag = document.createElement('span');
            flag.innerHTML = '🚩';
            target.classList.add('flagged');
            target.classList.remove('unrevealed');
            target.appendChild(flag);
            document.getElementById('flag-count').innerHTML = ++flag_count;
        }
        else if (target.children[0].innerHTML == '🚩') {
            let flag = target.children[0];
            target.removeChild(flag);
            document.getElementById('flag-count').innerHTML = --flag_count;
        }

        board_view.children[row].children[col].classList.add('selected-cell');

        return;
    }

    left_mouse_click(row, col);
    board_view.children[row].children[col].classList.add('selected-cell');
}

function touches_clicked_cell(cr, cc, r, c) {
    return (r == cr-1 && c == cc-1) ||
           (r == cr-1 && c == cc) ||
           (r == cr-1 && c == cc+1) ||
           (r == cr && c == cc-1) ||
           (r == cr && c == cc) ||
           (r == cr && c == cc+1) ||
           (r == cr+1 && c == cc-1) ||
           (r == cr+1 && c == cc) ||
           (r == cr+1 && c == cc+1);
}

// returns a 2d array where each element is an int 0-8, or 💣
function build_board(height, width, mines, clicked_row, clicked_col) {
    let board = [];
    
    // build matrix
    for (let i = 0; i < height; ++i) {
        let row = []    

        for (let j = 0; j < width; ++j) {
            row.push('');
        }

        board.push(row);
    }

    board[clicked_row][clicked_col] = 0;

    // randomly place mines
    for (let i = 0; i < mines; ++i) {
        while (true) {
            let row = Math.floor(Math.random() * height)
            let col = Math.floor(Math.random() * width);

            // if its already a bomb or if it touches the clicked cell try again
            // nb: this relies on only board[clicked_row][clicked_col] to be the only element set
            if (board[row][col] != '' || touches_clicked_cell(clicked_row, clicked_col, row, col)) {
                continue;
            }
            else {
                board[row][col] = '💣';
                break;
            }
        }
    }

    // update mine counts
    for (let i = 0; i < height; ++i) {
        for (let j = 0; j < width; ++j) {

            if (board[i][j] == '💣') {
                continue;
            }

            let num_mines = 0;

            if (i > 0 && board[i-1][j] == '💣') {
                num_mines += 1;
            }
            if (i < height-1 && board[i+1][j] == '💣') {
                num_mines += 1;
            }
            if (j > 0 && board[i][j-1] == '💣') {
                num_mines += 1;
            }
            if (j < width-1 && board[i][j+1] == '💣') {
                num_mines += 1;
            }
            if (i > 0 && j > 0 && board[i-1][j-1] == '💣') {
                num_mines += 1;
            }
            if (i > 0 && j < width-1 && board[i-1][j+1] == '💣') {
                num_mines += 1;
            }
            if (i < height-1 && j < width-1 && board[i+1][j+1] == '💣') {
                num_mines += 1;
            }
            if (i < height-1 && j > 0 && board[i+1][j-1] == '💣') {
                num_mines += 1;
            }

            board[i][j] = num_mines;
        }
    }

    return board;
}

function timer() {
    t = setTimeout(add_time, 1000);
}

function format_time(x) {
    return (x > 0 ? (x > 9 ? x : '0' + x) : '00')
}

function add_time() {
    seconds += 1;
    let clock = document.getElementById('timer');

    if (seconds >= 60) {
        minutes += 1;
        seconds = 0;
    }

    clock.innerHTML = format_time(minutes) + ':' + format_time(seconds);

    timer();
}