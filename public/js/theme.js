// update theme
let theme = localStorage.getItem('theme');
let is_pink = (theme == 'pink');
document.body.style.backgroundColor = (is_pink ? '#ffcfdc' : '#D8DEE9');
document.body.style.color = (is_pink ? '#ca7b80' : '#434C5E');
Array.from(document.getElementsByTagName('button')).map(button => button.style.color = (is_pink ? '#696969' : '#434C5E'));
