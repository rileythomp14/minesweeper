function cell_char(start, val) {
    return String.fromCharCode(start.charCodeAt(0) + val);
}

module.exports = {
    
    // represent the board object as a string to save in db
    build_board_str: function(height, width, board) {
        board_str = '';

        for (let y = 0; y < height; ++y) {
            for (let x = 0; x < width; ++x) {
                let cell = board[y][x];

                // if cell is a bomb. set value to -1
                if (isNaN(cell.val)) {
                    cell.val = -1;
                }

                // correctly flagged cells are an x
                if (cell.flagged && cell.val == -1) {
                    board_str += 'x';
                }
                // incorecctly flagged cells are a capital X followed by its char value
                else if (cell.flagged) {
                    board_str += 'X';
                    board_str += cell_char('A', cell.val);
                }
                // revealed cells are their lower case char value
                else if (cell.revealed) {
                    board_str += cell_char('a', cell.val);
                }
                // unrevealed cells are their upper case char value (@ in the case for bombs)
                else {
                    board_str += cell_char('A', cell.val);
                }
            }
        }

        return board_str;
    }
}