'use strict'

let ms_utils = require('./ms_utils.js');
const { Client } = require('pg');
const { v1: uuidv1 } = require('uuid');
let express = require('express');
let app = express();
const PORT = process.env.PORT || 3000;

app.use(express.urlencoded());
app.use(express.json());
app.use(express.static(__dirname + '/public'));

const connectionString = process.env.DATABASE_URL;
const client = new Client();
client.connect();

app.listen(PORT, () => {
    console.log('Running on port ' + PORT);
});

app.get('/get_saved_game', async (req, res) => {
    let name = req.query.name;

    const saved_game_select = 'SELECT game_name, game_str, width, height, time, mines FROM saved_games WHERE game_name = $1;';

    const saved_game = await client.query(saved_game_select, [name]);

    res.send(saved_game);
})

app.get('/get_leaderboards', async (req, res) => {
    const range = -1*Number(req.query.range);

    const high_scores_select = 'SELECT username, time, difficulty, date FROM high_scores WHERE level = $1 AND date BETWEEN NOW()+($2 * interval \'1 day\') AND NOW() ORDER BY time ASC, difficulty DESC LIMIT 10;';

    const easy = await client.query(high_scores_select, [1, range]);
    const intermediate = await client.query(high_scores_select, [2, range]);
    const expert = await client.query(high_scores_select, [3, range]);

    let data = {
        easy: easy.rows,
        intermediate: intermediate.rows,
        expert: expert.rows
    };
    
    res.send(data);
});

app.get('/is_highscore', async (req, res) => {
    let level = req.query.level;
    let time = Number(req.query.time);

    const worst_hiscore_select = 'SELECT time FROM high_scores WHERE level = $1 AND date BETWEEN NOW()+($2 * interval \'1 day\') AND NOW() ORDER BY time ASC, difficulty DESC LIMIT 1 OFFSET 9;';

    const worst_hiscore_alltime = await client.query(worst_hiscore_select, [level, -3650]);
    const worst_hiscore_year = await client.query(worst_hiscore_select, [level, -365]);
    const worst_hiscore_month = await client.query(worst_hiscore_select, [level, -30]);
    const worst_hiscore_week = await client.query(worst_hiscore_select, [level, -7]);

    let is_highscore = worst_hiscore_alltime.rows.length == 0 || worst_hiscore_alltime.rows[0].time > time ||
                       worst_hiscore_year.rows.length == 0 || worst_hiscore_year.rows[0].time > time ||
                       worst_hiscore_month.rows.length == 0 || worst_hiscore_month.rows[0].time > time ||
                       worst_hiscore_week.rows.length == 0 || worst_hiscore_week.rows[0].time > time;

    res.send(is_highscore);
});


app.post('/add_game', async (req, res) => {
    const high_score_insert = 'INSERT INTO high_scores (id, username, time, difficulty, level, date) VALUES ($1, $2, $3, $4, $5, NOW());';

    try {
        await client.query(high_score_insert, [uuidv1(), req.body.name, req.body.time, req.body.difficulty, req.body.level]);
        res.sendStatus(200);
    }
    catch (err) {
        res.send(err);
    }
});

app.post('/update_game', async (req, res) => {
    let name = req.body.name;
    let time = Number(req.body.time);
    let board = JSON.parse(req.body.board);
    let height = board.length;
    let width = board[0].length;
    let board_str = ms_utils.build_board_str(height, width, board);

    const saved_game_update = 'UPDATE saved_games SET game_str = $1, time = $2, date = NOW() WHERE  game_name = $3;';

    try {
        await client.query(saved_game_update, [board_str, time, name]);
        res.sendStatus(200);
    }
    catch (err) {
        res.send(err);
    }    
});

app.post('/save_game', async (req, res) => {
    let name = req.body.name;
    let mines = Number(req.body.mines);
    let time = Number(req.body.time);
    let board = JSON.parse(req.body.board);
    let height = board.length;
    let width = board[0].length;
    let board_str = ms_utils.build_board_str(height, width, board);

    const save_game_insert = 'INSERT INTO saved_games (id, game_name, game_str, width, height, time, date, mines, date_created) VALUES($1, $2, $3, $4, $5, $6, NOW(), $7, NOW());';

    try {
        await client.query(save_game_insert, [uuidv1(), name, board_str, width, height, time, mines]);
        res.sendStatus(200);
    }
    catch (err) {
        res.send(err);
    }    
});
