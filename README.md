# minesweeper

https://mine-sweeper-js.herokuapp.com/

A full-featured minesweeper game with:
* Ability to save and resume playing games
* Default and custom difficulty levels
* Leaderboards with level, time and difficulty statistics
* Multiple colour themes
* A hint button that reveals a non-mine square along the border of the revealed squares
* A solve button that reveals or flags deducible squares in one pass from right to left, top to bottom

![minesweeper](minesweeper.png)
![pinkms](pinkms.png)

To run locally:

```$ git clone https://gitlab.com/rileythomp14/minesweeper.git```

```$ cd minesweeper```

```$ npm i```

```$ npm start```

Then go to localhost:3000 in a browser.

To configure db for local leaderboards:

* Fill in db.sh with appropriate values

```$ source db.sh```
